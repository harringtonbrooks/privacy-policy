<?php
/*
Plugin Name: OAG Privacy Policy
Plugin URI: http://wordpress.org/plugins/hello-dolly/
Description: This plugin allows the site to load the privacy policy from a centralised location. Note that the plugin version number is not the Privacy Policy version number.
Author: Craig Shepherd
Version: 0.2
Author URI: http://www.craigshepherd.co.uk
BitBucket Plugin URI: https://bitbucket.org/harringtonbrooks/privacy-policy
BitBucket Branch:     master
*/

add_shortcode('privacy-policy', 'loadPrivacyPolicy');
function loadPrivacyPolicy()
{
    $dbName = 'oneadvice_wp';

    $wpdb = new wpdb(DB_USER, DB_PASSWORD, $dbName, DB_HOST);

    $wpdb->hide_errors();

    $privacy_policy_id = "3303";
    $result = $wpdb->get_row("SELECT * FROM $dbName.wrxwg9rsmw_posts WHERE ID = $privacy_policy_id", ARRAY_A);

    return $result['post_content'];
}

