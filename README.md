# Privacy Policy

This plugin allows the site to load the privacy policy from a centralised location. Note that the plugin version number is not the Privacy Policy version number.